package io.github.javaherobrine.chemistry;
public class Element {
	public Elements element;
	public int oxidizing;
	public int activity;
	public int[] valence;
	public Element(Elements element) {
		this.element=element;
	}
}
