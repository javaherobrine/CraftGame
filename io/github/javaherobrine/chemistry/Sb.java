package io.github.javaherobrine.chemistry;
import io.github.javaherobrine.*;
import io.github.javaherobrine.superScienceStudent.*;
public class Sb extends Element implements SuperScienceStudent{
	public static final String description=Constants.EASTER_EGG_ELEMENT_SB;
	public Sb() {
		super(Elements.Sb);
	}
}
