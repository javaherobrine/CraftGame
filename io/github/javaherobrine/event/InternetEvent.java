package io.github.javaherobrine.event;
public interface InternetEvent {
	void online();
	void offline();
}
