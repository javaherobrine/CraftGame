package io.github.javaherobrine.superScienceStudent;
import io.github.javaherobrine.*;
import io.github.javaherobrine.chemistry.*;
@EasterEgg
public class SuperScienceStudentElement extends Element implements SuperScienceStudent{
	public SuperScienceStudentElement() {
		super(Elements.SuperScienceElement);
	}
}
