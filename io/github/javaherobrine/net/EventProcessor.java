package io.github.javaherobrine.net;
public interface EventProcessor {
	void process(Object obj);
}
