package io.github.javaherobrine.net;
import java.io.*;
public class TCPOutputStream extends FilterOutputStream{
    public DataProcessor dataproc;
    public TCPOutputStream(OutputStream in) {
        this(in,PlainDataProcessor.DEFAULT_PROCESSOR);
    }
    public TCPOutputStream(OutputStream in,DataProcessor proc) {
        super(in);
        dataproc=proc;
    }
    public void writeData(byte[] bs) throws IOException {
        dataproc.write(this,bs);
    }
}